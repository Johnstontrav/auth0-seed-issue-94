'use strict';

angular.module('app', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'auth0'])
    .config(function ($stateProvider, $urlRouterProvider, authProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                template: '<h1>Seed - <a href="https://github.com/auth0/lock/issues/94" target="_blank">https://github.com/auth0/lock/issues/94</a></h1>'
            });

        authProvider.init({
            domain: 'samples.auth0.com',
            clientID: 'BUIJSW9x60sIHBw8Kd9EmCbj8eDIFxDC',
            loginUrl: '/'
        });

        $urlRouterProvider.otherwise('/');

    }).run(function (auth) {
        auth.hookEvents();
    });
